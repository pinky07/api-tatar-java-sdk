import com.uip.tatar.APITatar;
import com.uip.tatar.APITatarFactory;
import com.uip.tatar.config.Configuration;
import com.uip.tatar.config.ConfigurationBuilder;

/**
 * Author: Khamidullin Kamil
 * Date: 11.06.14
 * Time: 10:59
 */
public class APITatarProvider {
    private static APITatar apiTatar;

    static {
        Configuration configuration = new ConfigurationBuilder()
                .url("api.tatar.ru") //default
                .useSSL(true)
                .version("v1")  //default
                .authorizationToken("1d4c9c2443d0e902c2bb0aa98dd6221b87b71be81ffd1de4b7ce410f9872bf8a33c627d48606baad")  //default
                .secretKey("f8dc24763533a2854c5947ca190169a35f1f41e2077b39ff15aeb6a10ee3b5c8219815f56e66746f")  //default
                .serverDoesNotRespondErrorText("Сервер не отвечает") //default
                .wrongServerResponseErrorText("Неправильный формат ответа сервера") //default
                .build();

        apiTatar = APITatarFactory.getInstance(configuration);
    }


    public static APITatar get() {
        return apiTatar;
    }
}
