import com.uip.tatar.APITatar;
import com.uip.tatar.APITatarException;
import com.uip.tatar.model.history.AppointmentHistories;
import com.uip.tatar.model.history.Paginate;
import com.uip.tatar.model.history.PaymentHistories;
import com.uip.tatar.model.user.User;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;


/**
 * Author: Khamidullin Kamil
 * Date: 11.06.14
 * Time: 10:37
 */
public class UserRegistryTest {
    final static String LOGIN = "9518976217";
    final static String PASSWORD = "1";
    final static APITatar APITatar;

    static {
        APITatar = APITatarProvider.get();
    }

    private void authenticatedIfNeed() throws APITatarException {
        if(!APITatar.isAuthenticated()) {
            APITatar.auth(LOGIN, PASSWORD);
        }
    }

    private User getTestUser() {
        try {
            User user = new User();
            Field field = User.class.getDeclaredField("id");
            field.setAccessible(true);
            field.set(user, "50d308f5dc490ef10e00000d");

            return user;
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @Test
    public void authApiTatarTest() {
        try {
            APITatar.auth(LOGIN, PASSWORD);
            Assert.assertNotNull("Session is null", APITatar.getSession());
        } catch (APITatarException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void sendSMSTest() {
        APITatar apiTatar  = APITatarProvider.get();
        try {
            apiTatar.auth(LOGIN, PASSWORD);
            apiTatar.sendSMS("50d308f5dc490ef10e00000d", "test");
        }  catch (APITatarException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void createUser() {
        try {
            User user = APITatar.createUser("9376234232", "123");
            Assert.assertNotNull("User is null", user);
            APITatar.removeUser(user.getId());
        } catch (APITatarException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void getUser() {
        try {
            authenticatedIfNeed();

            User user = APITatar.getUser(LOGIN, PASSWORD);

            Assert.assertNotNull("User is null", user);
        }  catch (APITatarException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void getAppointmentHistories() {
        try {
            authenticatedIfNeed();
            AppointmentHistories appointmentHistories = APITatar.getAppointmentHistories(getTestUser().getId(), new Paginate());
            Assert.assertNotNull("appointmentHistories is null", appointmentHistories);
        }  catch (APITatarException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void getPaymentHistories() {
        try {
            authenticatedIfNeed();
            PaymentHistories appointmentHistories = APITatar.getPaymentHistories(getTestUser().getId(), new Paginate());
            Assert.assertNotNull("paymentHistories is null", appointmentHistories);
        }  catch (APITatarException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}
