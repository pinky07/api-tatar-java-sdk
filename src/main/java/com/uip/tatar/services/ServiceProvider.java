package com.uip.tatar.services;

import com.uip.tatar.DefaultAPITatarImpl;
import com.uip.tatar.api.APITatarDefaultClient;
import com.uip.tatar.auth.AuthSupport;

/**
 * Author: Khamidullin Kamil
 * Date: 18.06.14
 * Time: 11:34
 */
public interface ServiceProvider {
    public UserRegistryService createUserRegistryService(APITatarDefaultClient client);

    public GibddService createGibddService(APITatarDefaultClient client);

    public OperationHistoryService createOperationHistoryService(APITatarDefaultClient client);
}
