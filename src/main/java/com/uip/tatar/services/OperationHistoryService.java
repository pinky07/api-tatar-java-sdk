package com.uip.tatar.services;

import com.uip.tatar.APITatarException;
import com.uip.tatar.model.history.AppointmentHistories;
import com.uip.tatar.model.history.Paginate;
import com.uip.tatar.model.history.PaymentHistories;

/**
 * Author: Khamidullin Kamil
 * Date: 18.06.14
 * Time: 11:19
 */
public interface OperationHistoryService {
    /**
     * История записей в очередь
     * @param userId
     * @param paginate
     * @return
     * @throws APITatarException
     */
    public AppointmentHistories getAppointmentHistories(String userId, Paginate paginate) throws APITatarException;

    /**
     * История платежей
     * @param userId
     * @param paginate
     * @return
     * @throws APITatarException
     */
    public PaymentHistories getPaymentHistories(String userId, Paginate paginate) throws APITatarException;
}
