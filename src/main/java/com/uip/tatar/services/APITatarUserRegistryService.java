package com.uip.tatar.services;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.uip.tatar.APITatarException;
import com.uip.tatar.api.APITatarAuthRequest;
import com.uip.tatar.api.APITatarDefaultClient;
import com.uip.tatar.api.APITatarRequest;
import com.uip.tatar.model.factory.UserModelFactory;
import com.uip.tatar.model.history.AppointmentHistories;
import com.uip.tatar.model.history.Paginate;
import com.uip.tatar.model.history.PaymentHistories;
import com.uip.tatar.model.user.*;
import com.uip.tatar.network.HttpQueryParams;
import com.uip.tatar.network.JsonHttpEntity;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 30.05.14
 * Time: 17:06
 */
public class APITatarUserRegistryService
        implements UserRegistryService, OperationHistoryService {

    public APITatarDefaultClient client;

    public final static String USERS_SECTION = "users";


    public APITatarUserRegistryService(APITatarDefaultClient authClient) {
        this.client = authClient;
    }

    /**
     * @param username
     * @param password
     * @return
     * @throws APITatarException
     */
    @Override
    public User getUser(String username, String password) throws APITatarException{
        HttpQueryParams params = new HttpQueryParams();
        params.put("username", username);
        params.put("password", password);

        return client.get(
                new APITatarAuthRequest(USERS_SECTION, params),
                new UserModelFactory(true)
        );
    }

    /**
     * @param user
     * @return
     * @throws APITatarException
     */
    @Override
    public User updateUser(User user) throws APITatarException {
        final List<String> exclusionFields = java.util.Arrays.asList(
                "id", "open_gov_id", "username", "password",
                "medical_record", "region_number"
        );

        String json = new GsonBuilder()
            .addSerializationExclusionStrategy(
                    new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return exclusionFields.contains(f.getName());
                }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
            .registerTypeAdapter(Vehicle.class, new UnsetableModelSerializer())
            .registerTypeAdapter(JkhAccount.class, new UnsetableModelSerializer())
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setDateFormat("yyyy-MM-dd")
            .create()
            .toJson(user);

        client.put(new APITatarAuthRequest(USERS_SECTION + "/" + user.getId(), new JsonHttpEntity(json)), null);

        user.clearUnsetData();

        return user;
    }

    /**
     * @param phoneNumber
     * @param password
     * @return
     * @throws APITatarException
     */
    @Override
    public User createUser(String phoneNumber, String password) throws APITatarException {
        JsonHttpEntity entity = new JsonHttpEntity();
        entity.put("username", phoneNumber);
        entity.put("password", password);

        return client.post(
                new APITatarRequest(USERS_SECTION, entity),
                new UserModelFactory(false)
        );
    }

    /**
     *
     * @param userId
     * @throws APITatarException
     */
    @Override
    public void removeUser(String userId) throws APITatarException {
        client.delete(new APITatarAuthRequest(USERS_SECTION + "/" + userId), null);
    }

    /**
     * @param userId
     * @param message
     * @throws APITatarException
     */
    @Override
    public void sendSMS(String userId, String message) throws APITatarException {
        JsonHttpEntity entity = new JsonHttpEntity();
        entity.put("type", "gosuslugi");
        entity.put("region_number", "16");
        entity.put("message", message);

        client.post(new APITatarRequest(USERS_SECTION + "/" + userId + "/sms", entity), null);
    }

    /**
     *
     * @param userId
     * @param paginate
     * @return
     */
    @Override
    public AppointmentHistories getAppointmentHistories(String userId, Paginate paginate) throws APITatarException {
        return client.get(
            new APITatarAuthRequest(USERS_SECTION + "/" + userId + "/operations/history/appointments"),
            new APITatarDefaultClient.DefaultResultHandler<AppointmentHistories>(AppointmentHistories.class)
        );
    }

    /**
     *
     * @param userId
     * @param paginate
     * @return
     */
    @Override
    public PaymentHistories getPaymentHistories(String userId, Paginate paginate) throws APITatarException {
        return client.get(
            new APITatarAuthRequest(USERS_SECTION + "/" + userId + "/operations/history/payments"),
            new APITatarDefaultClient.DefaultResultHandler<PaymentHistories>(PaymentHistories.class)
        );
    }
}
