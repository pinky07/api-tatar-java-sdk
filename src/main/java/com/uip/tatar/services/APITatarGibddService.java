package com.uip.tatar.services;

import com.uip.tatar.APITatarException;
import com.uip.tatar.api.APITatarAuthClient;
import com.uip.tatar.api.APITatarAuthRequest;
import com.uip.tatar.api.APITatarDefaultClient;
import com.uip.tatar.model.gibdd.GibddFines;
import com.uip.tatar.model.factory.GibddFinesModelFactory;
import com.uip.tatar.model.user.Vehicle;
import com.uip.tatar.network.HttpQueryParams;

/**
 * Author: Khamidullin Kamil
 * Date: 05.02.14
 * Time: 14:42
 */
public class APITatarGibddService implements GibddService {
    private APITatarDefaultClient client;

    public APITatarGibddService(APITatarDefaultClient client) {
        this.client = client;
    }

    @Override
    public GibddFines getFines(Vehicle vehicle) throws APITatarException {
        HttpQueryParams params = new HttpQueryParams();
        params.put("vehicle[number]", vehicle.getLatinizeNumber());
        params.put("vehicle[region]", vehicle.getRegion());
        params.put("vehicle[document_number]", vehicle.getPassport());


        return client.get(
                new APITatarAuthRequest("services/gibdd/fines", params),
                new GibddFinesModelFactory()
        );
    }
}
