package com.uip.tatar.services;

import com.uip.tatar.APITatarException;
import com.uip.tatar.model.user.User;

/**
 * Author: Khamidullin Kamil
 * Date: 30.05.14
 * Time: 17:04
 */
public interface UserRegistryService {
    /**
     * Получение данных пользователя
     * @param username
     * @param password
     * @return
     * @throws APITatarException
     */
    User getUser(String username, String password) throws APITatarException;

    /**
     * Обновление данных пользователя
     * @param user
     * @return
     * @throws APITatarException
     */
    User updateUser(User user) throws APITatarException;

    /**
     * Создание новго пользователя
     * @param phoneNumber
     * @param password
     * @return
     * @throws APITatarException
     */
    User createUser(String phoneNumber, String password) throws APITatarException;

    /**
     * Удаление пользователя
     * @param userId
     * @throws APITatarException
     */
    void removeUser(String userId) throws APITatarException;

    /**
     * Отправка смс конкретному пользователю
     * @param userId
     * @param message
     * @throws APITatarException
     */
    void sendSMS(String userId, String message) throws APITatarException;


}
