package com.uip.tatar.services;

import com.uip.tatar.APITatarException;
import com.uip.tatar.model.gibdd.GibddFines;
import com.uip.tatar.model.user.Vehicle;

/**
 * Author: Khamidullin Kamil
 * Date: 29.05.14
 * Time: 17:23
 */
public interface GibddService {
    GibddFines getFines(Vehicle vehicle) throws APITatarException;
}
