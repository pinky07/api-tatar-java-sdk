package com.uip.tatar;

import com.uip.tatar.api.APITatarDefaultClient;
import com.uip.tatar.config.Configuration;
import com.uip.tatar.services.*;

/**
 * Author: Khamidullin Kamil
 * Date: 29.05.14
 * Time: 17:15
 */
public class APITatarFactory {
    public static APITatar getInstance(Configuration configuration) {
        return new DefaultAPITatarImpl(configuration, new DefaultServiceProvider(), null);
    }

    final protected static class DefaultServiceProvider implements ServiceProvider {
        @Override
        public UserRegistryService createUserRegistryService(APITatarDefaultClient client) {
            return new APITatarUserRegistryService(client);
        }

        @Override
        public GibddService createGibddService(APITatarDefaultClient client) {
            return new APITatarGibddService(client);
        }

        @Override
        public OperationHistoryService createOperationHistoryService(APITatarDefaultClient client) {
            return new APITatarUserRegistryService(client);
        }
    }
}
