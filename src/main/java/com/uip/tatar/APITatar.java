package com.uip.tatar;

import com.uip.tatar.auth.AuthSupport;
import com.uip.tatar.services.GibddService;
import com.uip.tatar.services.OperationHistoryService;
import com.uip.tatar.services.UserRegistryService;

/**
 * Author: Khamidullin Kamil
 * Date: 29.05.14
 * Time: 16:50
 */
public interface APITatar extends
        AuthSupport,
        UserRegistryService,
        GibddService,
        OperationHistoryService {}
