package com.uip.tatar.config;


/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 17:11
 */
public class PropertyConfigurationFactoryImpl implements ConfigurationFactory {
    private static final PropertyConfiguration ROOT_CONFIGURATION;

    static {
        ROOT_CONFIGURATION = new PropertyConfiguration();
    }

    @Override
    public Configuration getInstance() {
        return ROOT_CONFIGURATION;
    }
}
