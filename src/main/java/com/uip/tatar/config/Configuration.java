package com.uip.tatar.config;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 17:02
 */
public interface Configuration {
    String getVersion();
    String getHost();
    boolean isEnableSSL();
    String getRestBaseURL();
    String getAuthorizationToken();
    String getSecretKey();
    String getServerDoesNotRespondDefaultErrorText();
    String getWrongServerResponseDefaultErrorText();
    boolean isDebug();
}
