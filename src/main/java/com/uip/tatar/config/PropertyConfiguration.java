package com.uip.tatar.config;

import java.io.*;
import java.util.Properties;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 13:00
 */
public class PropertyConfiguration extends BaseConfiguration {
    public final static String DEBUG = "debug";
    public final static String AUTH_CONSUMER_KEY = "auth.consumerToken";
    public final static String AUTH_CONSUMER_SECRET = "auth.consumerSecret";
    public final static String HOST = "host";
    public final static String VERSION = "version";
    public final static String HTTP_USE_SSL = "http.useSSL";

    public final static String API_TATAR_PROPERTIES = "api_tatar.properties";

    public PropertyConfiguration() {
        final Properties properties = new Properties();
        //  ./api_tatar.properties in the classpath
        loadProperties(properties, "." + File.separatorChar + API_TATAR_PROPERTIES);
        //  /api_tatar.properties in the classpath
        loadProperties(properties, Configuration.class.getResourceAsStream("/" + API_TATAR_PROPERTIES));
        //  /WEB/INF/api_tatar.properties in the classpath
        loadProperties(properties, Configuration.class.getResourceAsStream("/WEB-INF/" + API_TATAR_PROPERTIES));
        // for Google App Engine
        try {
            loadProperties(properties, new FileInputStream("WEB-INF/" + API_TATAR_PROPERTIES));
        } catch (SecurityException ignore) {
        } catch (FileNotFoundException ignore) {
        }

        setFields(properties);
    }

    private void setFields(Properties properties) {
        if (hasProperty(properties, DEBUG)) {
            setDebug(getBoolean(properties, DEBUG));
        }
        if (hasProperty(properties, HOST)) {
            setHost(getString(properties, HOST));
        }
        if(hasProperty(properties, VERSION)) {
            setVersion(getString(properties, VERSION));
        }
        if(hasProperty(properties, HTTP_USE_SSL)) {
            setUseSSL(getBoolean(properties, HTTP_USE_SSL));
        }
        if(hasProperty(properties, AUTH_CONSUMER_KEY)) {
            setAuthorizationToken(getString(properties, AUTH_CONSUMER_KEY));
        }
        if(hasProperty(properties, AUTH_CONSUMER_SECRET)) {
            setSecretKey(getString(properties, AUTH_CONSUMER_SECRET));
        }
    }

    private boolean loadProperties(Properties props, String path) {
        FileInputStream fis = null;
        try {
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                fis = new FileInputStream(file);
                props.load(fis);
                return true;
            }
        } catch (Exception ignore) {
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ignore) {}
        }
        return false;
    }

    private boolean loadProperties(Properties props, InputStream is) {
        try {
            props.load(is);
            return true;
        } catch (Exception ignore) {
        }
        return false;
    }

    private boolean hasProperty(Properties props, String name) {
        return props.getProperty(name) != null;
    }

    protected boolean getBoolean(Properties props, String name) {
        String value = props.getProperty(name);
        return Boolean.valueOf(value);
    }

    protected int getIntProperty(Properties props, String name) {
        String value = props.getProperty(name);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return -1;
        }
    }

    protected String getString(Properties props, String name) {
        return props.getProperty(name);
    }
}
