package com.uip.tatar.config;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 17:08
 */
public class ConfigurationBuilder {
    protected BaseConfiguration configuration;

    public ConfigurationBuilder() {
        this.configuration = new PropertyConfiguration();
    }

    public ConfigurationBuilder version(String version) {
        this.configuration.setVersion(version);

        return this;
    }

    public ConfigurationBuilder url(String url) {
        this.configuration.setHost(url);

        return this;
    }

    public ConfigurationBuilder authorizationToken(String token) {
        this.configuration.setAuthorizationToken(token);

        return this;
    }

    public ConfigurationBuilder secretKey(String secretKey) {
        this.configuration.setSecretKey(secretKey);

        return this;
    }

    public ConfigurationBuilder wrongServerResponseErrorText(String errorText) {
        this.configuration.setWrongServerResponseDefaultErrorText(errorText);

        return this;
    }

    public ConfigurationBuilder serverDoesNotRespondErrorText(String errorText) {
        this.configuration.setServerDoesNotRespondDefaultErrorText(errorText);

        return this;
    }

    public ConfigurationBuilder useSSL(boolean value) {
        this.configuration.setUseSSL(value);

        return this;
    }

    public ConfigurationBuilder debug(boolean value) {
        this.configuration.setDebug(value);

        return this;
    }

    public Configuration build() {
        return this.configuration;
    }
}
