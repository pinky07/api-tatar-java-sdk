package com.uip.tatar.config;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 17:10
 */
public interface ConfigurationFactory {
    public Configuration getInstance();
}
