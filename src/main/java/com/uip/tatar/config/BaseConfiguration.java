package com.uip.tatar.config;

/**
 * Author: Khamidullin Kamil
 * Date: 25.12.13
 * Time: 10:02
 */
public class BaseConfiguration implements Configuration {
    private boolean enableSSL;
    private boolean isDebug;
    private String host;
    private String version;
    private String authorizationToken/* = "1d4c9c2443d0e902c2bb0aa98dd6221b87b71be81ffd1de4b7ce410f9872bf8a33c627d48606baad"*/;
    private String secretKey/* = "f8dc24763533a2854c5947ca190169a35f1f41e2077b39ff15aeb6a10ee3b5c8219815f56e66746f"*/;
    private String wrongServerResponseDefaultErrorText;
    private String serverDoesNotRespondDefaultErrorText;

    public BaseConfiguration() {
        setUseSSL(false);
        setDebug(false);
        setHost("api.tatar.ru");
        setVersion("v1");
        setWrongServerResponseDefaultErrorText("Неправильный формат ответа сервера");
        setServerDoesNotRespondDefaultErrorText("Сервер не отвечает");
    }

    public String getHost() {
        return host;
    }

    public boolean isEnableSSL() {
        return enableSSL;
    }

    protected void setUseSSL(boolean value) {
        this.enableSSL = value;
    }

    protected void setHost(String url) {
        this.host = url;
    }

    public String getVersion() {
        return version;
    }

    protected void setVersion(String version) {
        this.version = version;
    }

    public String getAuthorizationToken() {
        return authorizationToken;
    }

    protected void setAuthorizationToken(String authorizationToken) {
        this.authorizationToken = authorizationToken;
    }

    public String getSecretKey() {
        return secretKey;
    }

    protected void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getWrongServerResponseDefaultErrorText() {
        return wrongServerResponseDefaultErrorText;
    }

    protected void setWrongServerResponseDefaultErrorText(String wrongServerResponseDefaultErrorText) {
        this.wrongServerResponseDefaultErrorText = wrongServerResponseDefaultErrorText;
    }

    public String getServerDoesNotRespondDefaultErrorText() {
        return serverDoesNotRespondDefaultErrorText;
    }

    protected void setServerDoesNotRespondDefaultErrorText(String serverDoesNotRespondDefaultErrorText) {
        this.serverDoesNotRespondDefaultErrorText = serverDoesNotRespondDefaultErrorText;
    }

    protected void setDebug(boolean value) {
        this.isDebug = value;
    }

    public boolean isDebug() {
        return this.isDebug;
    }

    @Override
    public String getRestBaseURL() {
        //todo need impl
        return null;
    }
}
