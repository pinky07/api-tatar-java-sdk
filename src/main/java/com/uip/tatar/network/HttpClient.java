package com.uip.tatar.network;

import com.uip.tatar.APITatarException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 06.12.12
 * Time: 10:00
 */
public class HttpClient {
    HttpResponse mHttpResponse;
    private static final int TIMEOUT_SOCKET = 30000;
    private static final int TIMEOUT_CONNECTION = 20000;

    /**
     * Отправляет запрос на сервер
     * @param httpRequest
     * @return
     * @throws APITatarException
     */
    protected void sendRequest(HttpRequestBase httpRequest, Map<String, String> headers) throws APITatarException {
        try {
            if(headers != null) {
                for(Map.Entry<String, String> entry : headers.entrySet())
                    httpRequest.setHeader(entry.getKey(), entry.getValue());
            }

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setSoTimeout(httpParameters, TIMEOUT_SOCKET);
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIMEOUT_CONNECTION);
            org.apache.http.client.HttpClient httpClient = new DefaultHttpClient(httpParameters);
            mHttpResponse = httpClient.execute(httpRequest);
        } catch (IOException exception) {
            throw new APITatarException(exception);
        }
    }

    /**
     * Отправляет POST запрос
     * @param uri
     * @param content
     * @param headers
     * @return
     */
    public void post(String uri, HttpEntity content, Map<String, String> headers)
            throws APITatarException {
        HttpPost httpPost = new HttpPost(uri);
        if(content != null)
            httpPost.setEntity(content);

        sendRequest(httpPost, headers);
    }


    /**
     * Отправляет POST запрос
     * @param uri
     * @param content
     * @return
     */
    public void post(String uri, HttpEntity content)
            throws APITatarException {
        post(uri, content, null);
    }

    /**
     * Отправляет POST запрос
     * @param uri
     * @return
     */
    public void post(String uri)
            throws APITatarException {
        post(uri, null, null);
    }

    /**
     * Отправляет GET запрос
     * @param uri
     * @param headers
     * @return
     */
    public void get(String uri, Map<String, String> headers)
            throws APITatarException {
        HttpGet httpget = new HttpGet(uri);
        sendRequest(httpget, headers);
    }

    /**
     * Отправляет GET запрос
     * @param uri
     * @return
     */
    public void get(String uri)
            throws APITatarException {
        get(uri, null);
    }

    /**
     * Отправляет PUT запрос
     * @param uri
     * @param content
     * @param headers
     * @return
     */
    public void put(String uri, HttpEntity content, Map<String, String> headers)
            throws APITatarException {
        HttpPut httpPut = new HttpPut(uri);
        if(content != null)
            httpPut.setEntity(content);

        sendRequest(httpPut, headers);
    }

    /**
     * Отправляет PUT запрос
     * @param uri
     * @param content
     * @return
     */
    public void put(String uri, HttpEntity content)
            throws APITatarException {
        put(uri, content, null);
    }

    /**
     * Отправляет DELETE запрос
     * @param uri
     * @param headers
     * @return
     */
    public void delete(String uri, Map<String, String> headers)
            throws APITatarException {
        HttpDelete httpDelete = new HttpDelete(uri);
        sendRequest(httpDelete, headers);
    }

    /**
     * Отправляет DELETE запрос
     * @param uri
     * @return
     */
    public void delete(String uri)
            throws APITatarException {
        delete(uri, null);
    }

    /**
     * Возвращает ответ
     *
     * @return
     */
    public HttpResponse getResponse() {
        return mHttpResponse;
    }

    /**
     * Возвращает код ответа
     * @return
     */
    public StatusLine getResponseStatus() {
        return mHttpResponse.getStatusLine();
    }
}
