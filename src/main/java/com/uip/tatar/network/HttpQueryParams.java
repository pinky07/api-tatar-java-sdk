package com.uip.tatar.network;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Khamidullin Kamil
 * Date: 23.01.13
 * Time: 14:21
 */
public class HttpQueryParams extends HashMap<String, String> {

    public String getQueryString()/* throws UnsupportedEncodingException*/ {
        StringBuilder q = new StringBuilder();
        int i           = 0;

        for (Map.Entry<String,String> entrySet : this.entrySet()) {
            String pattern  = (i == 0)? "?%s=%s" : "&%s=%s";
            String key      = entrySet.getKey();
            String value    = entrySet.getValue();

            if(key != null && value != null) {
                try { value = URLEncoder.encode(value, "UTF-8"); } catch (UnsupportedEncodingException uex) {/* utf -8 encoding always available*/}

                q.append(String.format(pattern, key, value));
            } i++;
        }

        return q.toString();
    }

    public static HttpQueryParams forPair(String key, String value) {
        HttpQueryParams httpQueryParams = new HttpQueryParams();
        httpQueryParams.put(key, value);

        return httpQueryParams;
    }

    public static HttpQueryParams forPair(String key, int value) {
        HttpQueryParams httpQueryParams = new HttpQueryParams();
        httpQueryParams.put(key, value);

        return httpQueryParams;
    }

    public void put(String key, int value) {
        put(key, Integer.toString(value));
    }

    public void put(String key, double value) {
        put(key, Double.toString(value));
    }

    public void put(String key, float value) {
        put(key, Float.toString(value));
    }

    public void put(String key, boolean value) {
        put(key, (value) ? "true" : "false");
    }
}
