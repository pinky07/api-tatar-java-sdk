package com.uip.tatar.network;

import com.uip.tatar.APITatarException;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 18.01.13
 * Time: 18:58
 */
public class NoInternetConnectionException extends APITatarException {

    public NoInternetConnectionException() {

    }

    public NoInternetConnectionException(String message) { super(message); }
}