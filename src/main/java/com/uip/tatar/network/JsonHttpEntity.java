package com.uip.tatar.network;

import com.uip.tatar.APITatarException;
import com.uip.tatar.api.APITatarConstants;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Collection;

/**
 * Author: Khamidullin Kamil
 * Date: 22.01.13
 * Time: 13:05
 * todo реализовать возможность создавать многовложенную структуру
 */
public class JsonHttpEntity extends AbstractHttpEntity implements Cloneable {

    protected String charset;
    protected final JSONObject object;

    public JsonHttpEntity(final JSONObject s, String charset) {
        super();
        if (s == null) {
            throw new IllegalArgumentException("Source string may not be null");
        }
        if (charset == null) {
            charset = APITatarConstants.CHARSET_UTF8;
        }

        object = s;
        setCharset(charset);
    }

    public JsonHttpEntity(final JSONObject s) {
        this(s, null);
    }

    public JsonHttpEntity(final String string) {
        this(new JSONObject(string), null);
    }

    public JsonHttpEntity() {
        this(new JSONObject());
    }

    public void setCharset(String charset) {
        this.charset = charset;
        setContentType(HTTP.PLAIN_TEXT_TYPE + HTTP.CHARSET_PARAM + charset);
    }

    public JsonHttpEntity put(String key, boolean value) {
        try {
            object.put(key, value);
        } catch (JSONException jex) {
            throw new IllegalArgumentException(jex);
        }

        return this;
    }

    public JsonHttpEntity put(String key, Collection value) {
        try {
            object.put(key, value);
        } catch (JSONException jex) {
            throw new IllegalArgumentException(jex);
        }

        return this;
    }

    public JsonHttpEntity put(String key, String value) {
        try {
            object.put(key, value);
        } catch (JSONException jex) {
            throw new IllegalArgumentException(jex);
        }

        return this;
    }

    public JsonHttpEntity put(String key, int value) {
        try {
            object.put(key, value);
        } catch (JSONException jex) {
            throw new IllegalArgumentException(jex);
        }

        return this;
    }

    public JsonHttpEntity put(String key, long value) {
        try {
            object.put(key, value);
        } catch (JSONException jex) {
            throw new IllegalArgumentException(jex);
        }

        return this;
    }

    public JsonHttpEntity put(String key, double value) {
        try {
            object.put(key, value);
        } catch (JSONException jex) {
            throw new IllegalArgumentException(jex);
        }

        return this;
    }

    public boolean isRepeatable() {
        return true;
    }

    public long getContentLength() {

        try {
            return object.toString().getBytes(charset).length;
        } catch (UnsupportedEncodingException e) {
            return 0;
        }

    }

    public InputStream getContent() throws IOException {
        return new ByteArrayInputStream(object.toString().getBytes(charset));
    }

    public void writeTo(final OutputStream outstream) throws IOException {
        if (outstream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        outstream.write(object.toString().getBytes(charset));
        outstream.flush();
    }

    /**
     * @return <code>false</code>
     */
    public boolean isStreaming() {
        return false;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return object.toString();
    }
}
