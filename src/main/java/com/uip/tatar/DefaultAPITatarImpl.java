package com.uip.tatar;

import com.uip.tatar.api.APITatarAuthClient;
import com.uip.tatar.auth.AuthSupport;
import com.uip.tatar.auth.Authorization;
import com.uip.tatar.auth.DefaultAuthorization;
import com.uip.tatar.auth.NullAuthorization;
import com.uip.tatar.config.Configuration;
import com.uip.tatar.model.gibdd.GibddFines;
import com.uip.tatar.model.Session;
import com.uip.tatar.model.history.AppointmentHistories;
import com.uip.tatar.model.history.Paginate;
import com.uip.tatar.model.history.PaymentHistories;
import com.uip.tatar.model.user.User;
import com.uip.tatar.model.user.Vehicle;
import com.uip.tatar.services.*;
import com.uip.tatar.util.StringUtils;

/**
 * Author: Khamidullin Kamil
 * Date: 05.02.14
 * Time: 14:40
 */
public class DefaultAPITatarImpl implements APITatar {
    private Configuration conf;
    private Authorization auth;
    private APITatarAuthClient client;

    private UserRegistryService usersResources;
    private OperationHistoryService operationHistoryService;
    private GibddService gibddResources;

    public DefaultAPITatarImpl(Configuration configuration, ServiceProvider serviceProvider, Authorization authorization) {
        conf = configuration;
        auth = authorization;
        init();

        usersResources = serviceProvider.createUserRegistryService(resourceProvider());
        operationHistoryService = serviceProvider.createOperationHistoryService(resourceProvider());
        gibddResources = serviceProvider.createGibddService(resourceProvider());
    }

    private APITatarAuthClient resourceProvider() {
        if(client == null) {
            client = new APITatarAuthClient(conf, auth);
        }

        return client;
    }

    public AuthSupport getAuthenticator() {
        if(!(auth instanceof AuthSupport)) {
            throw new IllegalStateException("Consumer token/secret combination not supplied");
        }

        return (AuthSupport)auth;
    }

    private void init() {
        if(auth == null) {
            String authorizationToken = conf.getAuthorizationToken();
            String secretKey = conf.getSecretKey();
            if (!StringUtils.isEmpty(authorizationToken) && !StringUtils.isEmpty(secretKey)) {
                this.auth = new DefaultAuthorization(conf);
            } else {
                this.auth = NullAuthorization.getInstance();
            }
        }
    }

    @Override
    public void setCredentials(String username, String password) {
        getAuthenticator().setCredentials(username, password);
    }

    @Override
    public Session auth(String username, String password) throws APITatarException {
        return getAuthenticator().auth(username, password);
    }

    @Override
    public boolean isAuthenticated() {
        return getAuthenticator().isAuthenticated();
    }

    @Override
    public void updateSession() throws APITatarException {
        getAuthenticator().updateSession();
    }

    @Override
    public Session getSession() throws APITatarException {
        return getAuthenticator().getSession();
    }

    @Override
    public void setSession(Session session) {
        getAuthenticator().setSession(session);
    }

    @Override
    public User getUser(String username, String password) throws APITatarException {
        return usersResources.getUser(username, password);
    }

    @Override
    public User updateUser(User user) throws APITatarException {
        return usersResources.updateUser(user);
    }

    @Override
    public User createUser(String phoneNumber, String password) throws APITatarException {
        return usersResources.createUser(phoneNumber, password);
    }

    @Override
    public void removeUser(String username) throws APITatarException {
        usersResources.removeUser(username);
    }

    @Override
    public void sendSMS(String userId, String message) throws APITatarException {
        usersResources.sendSMS(userId, message);
    }

    @Override
    public AppointmentHistories getAppointmentHistories(String userId, Paginate paginate) throws APITatarException {
        return operationHistoryService.getAppointmentHistories(userId, paginate);
    }

    @Override
    public PaymentHistories getPaymentHistories(String userId, Paginate paginate) throws APITatarException {
        return operationHistoryService.getPaymentHistories(userId, paginate);
    }

    @Override
    public GibddFines getFines(Vehicle vehicle) throws APITatarException {
        return gibddResources.getFines(vehicle);
    }
}
