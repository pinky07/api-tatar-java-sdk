package com.uip.tatar.auth;

import com.uip.tatar.APITatarException;
import com.uip.tatar.model.Session;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 10:03
 */
public interface AuthSupport {

    void setCredentials(String username, String password);

    Session auth(String username, String password) throws APITatarException;

    void updateSession() throws APITatarException;

    Session getSession() throws APITatarException;

    void setSession(Session session);

    boolean isAuthenticated();
}
