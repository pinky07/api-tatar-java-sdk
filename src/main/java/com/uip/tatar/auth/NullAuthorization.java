package com.uip.tatar.auth;

import com.uip.tatar.model.Session;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 18:27
 */
public class NullAuthorization implements Authorization {
    private static NullAuthorization INSTANCE = new NullAuthorization();

    private NullAuthorization() {}

    public static NullAuthorization getInstance() { return INSTANCE; }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public Session getSession() {
        throw new IllegalStateException("No session available");
    }
}
