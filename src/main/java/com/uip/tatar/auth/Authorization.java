package com.uip.tatar.auth;

import com.uip.tatar.model.Session;

/**
 * Author: Khamidullin Kamil
 * Date: 28.05.14
 * Time: 18:25
 */
public interface Authorization {
    boolean isEnabled();

    Session getSession();
}
