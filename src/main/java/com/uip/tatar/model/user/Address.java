package com.uip.tatar.model.user;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 04.07.13
 * Time: 15:55
 */
public class Address implements Serializable {
    private static final long serialVersionUID = 2211121541910284334L;
    protected String apartment;
    protected String regionName;
    protected String regionDistrictName;
    protected String cityName;
    protected String streetName;
    protected String house;
    protected String building;

    public String getRegionName() {
        return regionName;
    }

    public String getRegionDistrictName() {
        return regionDistrictName;
    }

    public String getCityName() {
        return cityName;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getHouse() {
        return house;
    }

    public String getBuilding() {
        return building;
    }

    public String getApartment() {
        return apartment;
    }
}
