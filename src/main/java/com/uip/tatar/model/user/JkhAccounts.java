package com.uip.tatar.model.user;

/**
 * Author: Khamidullin Kamil
 * Date: 30.05.14
 * Time: 17:02
 */
public class JkhAccounts extends UnsetableList<JkhAccount> {
    private static final long serialVersionUID = -2322283007735649714L;
    private transient JkhAccount defaultAccount;

    public JkhAccount getDefaultAccount() {
        if(defaultAccount == null && !this.isEmpty()) {
            for(JkhAccount jkhAccount : this) {
                if(jkhAccount.isDefault()) {
                    defaultAccount = jkhAccount;
                    return defaultAccount;
                }
            }

            defaultAccount = this.get(0);
        }

        return defaultAccount;
    }
}
