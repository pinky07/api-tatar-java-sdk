package com.uip.tatar.model.user;

/**
 * Author: Khamidullin Kamil
 * Date: 21.02.14
 * Time: 15:40
 */
public class MedicalPolicy {
    private String series;
    private String number;
    private String valid_until;
    private String place;
    private String date;

    public String getSeries() {
        return series;
    }
    public String getNumber() {
        return number;
    }
    public String getValidUntil() {
        return valid_until;
    }
    public String getPlace() {
        return place;
    }
    public String getDate() {
        return date;
    }
}
