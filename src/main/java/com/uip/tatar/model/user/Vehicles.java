package com.uip.tatar.model.user;

import java.util.LinkedList;

/**
 * Author: Khamidullin Kamil
 * Date: 30.05.14
 * Time: 14:51
 */
class Vehicles extends UnsetableList<Vehicle> {
    private static final long serialVersionUID = -7129593489919664525L;
    private transient Vehicle defaultVehicle;

    protected Vehicle getDefaultVehicle() {
        if(defaultVehicle == null && !this.isEmpty()) {
            for(Vehicle v : this) {
                if(v.isDefault()) {
                    defaultVehicle = v;
                    return defaultVehicle;
                }
            }

            defaultVehicle = this.get(0);
        }

        return defaultVehicle;
    }
}
