package com.uip.tatar.model.user;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 21.02.14
 * Time: 15:55
 */
public class Passport implements Serializable {
    private static final long serialVersionUID = -3584518340142701739L;
    protected String place;
    protected String date;
    protected String series;
    protected String number;

    public Passport(){}

    public String getPlace() {
        return place;
    }
    public String getDate() {
        return date;
    }
    public String getSeries() {
        return series;
    }
    public String getNumber() {
        return number;
    }
}
