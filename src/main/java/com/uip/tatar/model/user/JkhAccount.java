package com.uip.tatar.model.user;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 04.02.13
 * Time: 18:19
 */
public class JkhAccount implements Serializable, Unsetable {
    private static final long serialVersionUID = 8153345111590020364L;
    private String account_number;
    private String house_owner_surname;
    private String title;
    private String building;
    private String city_name;
    private String street_name;
    private String apartment;
    private String house;
    private String protection_account;
    private int is_line;
    private int informer_enable;
    private int is_default;

    private transient boolean unset = false;

    public String getHouseOwnerSurname() {
        return house_owner_surname;
    }

    public String getNumber() {
        return account_number;
    }

    public String getFlatNumber() {
        return apartment;
    }

    public String getTitle() {
        return title;
    }

    public String getBuilding() {
        return building;
    }

    public String getCityName() {
        return city_name;
    }

    public String getStreetName() {
        return street_name;
    }

    public String getHouse() {
        return house;
    }

    public String getProtectionAccount() {
        return protection_account;
    }

    public boolean isLine() {
        return (this.is_line == 1);
    }

    public void setInformerEnable(boolean value) {
        this.informer_enable = (value) ? 1 : 0;
    }

    public boolean isInformerEnable() {
        return (this.informer_enable == 1);
    }

    public void unset() {
        this.unset = true;
    }

    public boolean isUnset() {
        return this.unset;
    }

    public boolean isDefault() {
        return is_default == 1;
    }
}
