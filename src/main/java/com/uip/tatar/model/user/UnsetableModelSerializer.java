package com.uip.tatar.model.user;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Author: Khamidullin Kamil
 * Date: 11.03.14
 * Time: 10:53
 */
public class UnsetableModelSerializer<T extends Unsetable> implements JsonSerializer<T> {
    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        if(src.isUnset()) {
            return new JsonPrimitive("__unset");
        } else {
            return new Gson().toJsonTree(src);
        }
    }
}
