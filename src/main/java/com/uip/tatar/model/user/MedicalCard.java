package com.uip.tatar.model.user;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 16.08.13
 * Time: 12:01
 */
public class MedicalCard implements Serializable, Unsetable {
    private static final long serialVersionUID = 8489899330155856897L;
    private String patient_id;
    private String firstname;
    private String lastname;
    private String middlename;
    private String title;
    private String type;
    private String birthday;
    private String policy_series;
    private String policy_number;
    private String last_update;
    private boolean unset = false;

    public MedicalCard() {}

    public String getTitle() {
        return title;
    }

    public String getFirstname() {
        return firstname;
    }
    public String getLastname() {
        return lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getPolicySeries() {
        return policy_series;
    }

    public String getPolicyNumber() {
        return policy_number;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policy_number = policyNumber;
    }

    public String getPatientId() {
        return patient_id;
    }

    public void setPatientId(String patientId) {
        this.patient_id = patientId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLastUpdate() {
        return last_update;
    }

    public void setLastUpdate(String lastUpdate) {
        this.last_update = lastUpdate;
    }

    @Override
    public void unset() {
        unset = true;
    }

    @Override
    public boolean isUnset() {
        return unset;
    }

    @Override
    public String toString() {
        return String.format("%s %s", getFirstname(), getLastname());
    }
}
