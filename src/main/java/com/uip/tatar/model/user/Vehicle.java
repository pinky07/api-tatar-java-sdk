package com.uip.tatar.model.user;

import com.uip.tatar.util.StringUtils;

/**
 * Author: Khamidullin Kamil
 * Date: 30.01.13
 * Time: 18:09
 */
public class Vehicle implements Unsetable {
    protected String region;
    protected String number;
    protected String title;
    protected String passport;
    protected String type;
    protected int is_default;
    protected int informer_enable;

    protected transient boolean unset = false;

    public String getNumber() {
        return number;
    }

    public String getLatinizeNumber() {
        return StringUtils.latinizeAutoNumber(number);
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String regionCode) {
        this.region = regionCode;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String documentNumber) {
        this.passport = documentNumber;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public void setInformerEnable(boolean value) {
        this.informer_enable = (value) ? 1 : 0;
    }

    public boolean isInformerEnable() {
        return (this.informer_enable == 1);
    }

    public void unset() {
        this.unset = true;
    }

    public boolean isUnset() {
        return this.unset;
    }

    public boolean isDefault() {
        return is_default == 1;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", getTitle(), getNumber());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;

        if (informer_enable != vehicle.informer_enable) return false;
        if (unset != vehicle.unset) return false;
        if (number != null ? !number.equals(vehicle.number) : vehicle.number != null) return false;
        if (passport != null ? !passport.equals(vehicle.passport) : vehicle.passport != null) return false;
        if (region != null ? !region.equals(vehicle.region) : vehicle.region != null) return false;
        if (title != null ? !title.equals(vehicle.title) : vehicle.title != null) return false;
        if (type != null ? !type.equals(vehicle.type) : vehicle.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = region != null ? region.hashCode() : 0;
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (passport != null ? passport.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + informer_enable;
        result = 31 * result + (unset ? 1 : 0);
        return result;
    }
}
