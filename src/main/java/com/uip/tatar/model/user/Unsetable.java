package com.uip.tatar.model.user;

/**
 * Author: Khamidullin Kamil
 * Date: 30.05.14
 * Time: 11:24
 */
public interface Unsetable {
    public void unset();

    public boolean isUnset();
}
