package com.uip.tatar.model.user;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * User: Khamidullin Kamil
 * Date: 29.11.12
 * Time: 19:14
 */
public class User {
    protected String id;
    protected String username;
    protected String firstname;
    protected String lastname;
    protected String middlename;
    protected Date birthdate;
    protected String inn;
    protected String snils;
    protected String email;
    protected String region_number;
    protected Passport passport;
    protected Address address;
    protected MedicalPolicy medical_policy;
    protected MedicalCards medical_record = new MedicalCards();
    protected Vehicles vehicles = new Vehicles();
    protected Children children = new Children();
    protected JkhAccounts tsj = new JkhAccounts();

    protected Passport getOrCreatePassport() {
        if(this.passport == null) {
            this.passport = new Passport();
        }

        return this.passport;
    }

    protected Address getOrCreateAddress() {
        if(this.address == null) {
            this.address = new Address();
        }

        return this.address;
    }

    protected MedicalPolicy getOrCreateMedicalPolicy() {
        if(this.medical_policy == null) {
            this.medical_policy = new MedicalPolicy();
        }

        return this.medical_policy;
    }


    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstname;
    }

    public String getLastName() {
        return lastname;
    }

    public String getMiddleName() {
        return middlename;
    }

    public Date getBirthDate() {
        return birthdate;
    }

    public String getInn() {
        return inn;
    }

    public String getSNILS() {
        return snils;
    }

    public String getEmail() {
        return email;
    }

    public String getRegionNumber() {
        return region_number;
    }

    public String getPassportPlace() {
        return getOrCreatePassport().getPlace();
    }

    public String getPassportDate() {
        return getOrCreatePassport().getDate();
    }

    public String getPassportSeries() {
        return getOrCreatePassport().getSeries();
    }

    public String getPassportNumber() {
        return getOrCreatePassport().getNumber();
    }

    public String getRegion() {
        return getOrCreateAddress().getRegionName();
    }

    public String getDistrict() {
        return getOrCreateAddress().getRegionDistrictName();
    }

    public String getCity() {
        return getOrCreateAddress().getCityName();
    }

    public String getStreet() {
        return getOrCreateAddress().getStreetName();
    }

    public String getHouse() {
        return getOrCreateAddress().getHouse();
    }

    public String getApartment() {
        return getOrCreateAddress().getApartment();
    }

    public String getMedicalPolicySeries() {
        return getOrCreateMedicalPolicy().getSeries();
    }

    public String getMedicalPolicyNumber() {
        return getOrCreateMedicalPolicy().getNumber();
    }

    public String getMedicalPolicyValidUntil() {
        return getOrCreateMedicalPolicy().getValidUntil();
    }

    public String getMedicalPolicyPlace() {
        return getOrCreateMedicalPolicy().getPlace();
    }

    public String getMedicalPolicyDate() {
        return getOrCreateMedicalPolicy().getDate();
    }

    public Vehicle getDefaultVehicle() {
        return vehicles.getDefaultVehicle();
    }

    public JkhAccount getDefaultJkhAccount() {
        return tsj.getDefaultAccount();
    }

    public List<Vehicle> getVehicles() {
        return Collections.unmodifiableList(vehicles.getNotUnsetItems());
    }

    public JkhAccounts getJkhAccounts() {
        return tsj;
    }

    public List<Child> getChildren() {
        return children;
    }

    public MedicalCards getMedicalCards() {
        return this.medical_record;
    }


    /*********************************
     * setters
     ********************************/
    public void setLastName(String lastName) {
        this.lastname = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middlename = middleName;
    }

    public void setBirthDate(Date date) {
        this.birthdate = date;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public void setSNILS(String snils) {
        this.snils = snils;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String setPassportPlace(String place) {
        return getOrCreatePassport().place = place;
    }

    public String setPassportDate(String date) {
        return getOrCreatePassport().date = date;
    }

    public String setPassportSeries(String series) {
        return getOrCreatePassport().series = series;
    }

    public String setPassportNumber(String string) {
        return getOrCreatePassport().number = string;
    }

    public void setMedicalPolicy(MedicalPolicy medicalPolicy) {
        this.medical_policy = medicalPolicy;
    }

    public void addVehicle(Vehicle vehicle) {
        this.getVehicles().add(vehicle);
    }

    public void removeVehicle(Vehicle vehicle) {
        this.vehicles.unset(vehicle);
    }

    public void addJkhAccount(JkhAccount account) {
        this.getJkhAccounts().add(account);
    }

    public void addMedicalCard(MedicalCard medicalCard) {
        this.getMedicalCards().add(medicalCard);
    }


    /**
     * must be call after update model
     */
    public void clearUnsetData() {
        vehicles.removeUnsetItems();
        children.removeUnsetItems();
        tsj.removeUnsetItems();
        medical_record.removeUnsetItems();
    }
}
