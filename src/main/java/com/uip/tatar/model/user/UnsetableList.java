package com.uip.tatar.model.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.06.14
 * Time: 15:07
 */
public class UnsetableList<E extends Unsetable> extends LinkedList<E> {

    private static final long serialVersionUID = 1361171277075321919L;

    public boolean unset(E item) {
        int index = indexOf(item);
        if(index == -1) {
            return false;
        }

        get(index).unset();

        return true;
    }

    public List<E> getNotUnsetItems() {
        List<E> list = new ArrayList<E>();
        for(E item : this) {
            if(!item.isUnset()) {
                list.add(item);
            }
        }

        return list;
    }

    public void removeUnsetItems() {
        for(int i = 0; i < size(); i++) {
            E item = get(i);
            if(item.isUnset()) {
                remove(item);
            }
        }
    }
}
