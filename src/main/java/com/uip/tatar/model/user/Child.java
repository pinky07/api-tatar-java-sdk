package com.uip.tatar.model.user;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 22.04.13
 * Time: 19:37
 */
public class Child implements Serializable, Unsetable {
    private static final long serialVersionUID = 6403420463067221849L;
    private String firstname;
    private String lastname;
    private String middlename;
    //private String sex;
    private String mpolicy_series;
    private String mpolicy_number;
    private String document_number;
    private String document_series;
    private String edu_login;
    private String edu_password;
    private String snils;
    protected transient boolean unset = false;

    //private String birthdate;
    private int informer_enable;

    public String getFirstName() {
        return firstname;
    }
    public String getLastName() {
        return lastname;
    }
    public String getMiddleName() {
        return middlename;
    }
    public String getEduLogin() {
        return edu_login;
    }
    public String getEduPassword() {
        return edu_password;
    }

    /*public String getSex() {
        return sex;
    } */

    public String getMpolicySeries() {
        return mpolicy_series;
    }

    public String getMpolicyNumber() {
        return mpolicy_number;
    }

    public String getDocumentNumber() {
        return document_number;
    }

    public String getDocumentSeries() {
        return document_series;
    }

    /*public String getBirthdate() {
        return birthdate;
    }



    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public void setInformerEnable(boolean value) {
        this.informer_enable = (value) ? 1 : 0;
    }

    public boolean isInformerEnable() {
        return (this.informer_enable == 1);
    } */


    @Override
    public void unset() {
        unset = true;
    }

    @Override
    public boolean isUnset() {
        return unset;
    }

    @Override
    public String toString() {
        return String.format("%s %s", getFirstName(), getLastName());
    }
}