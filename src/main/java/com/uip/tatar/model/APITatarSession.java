package com.uip.tatar.model;


/**
 * Author: Khamidullin Kamil
 * Date: 04.07.13
 * Time: 22:41
 */
public class APITatarSession implements Session {
    protected String sessionToken;

    public APITatarSession(){}

    @Override
    public String getToken() {
        return sessionToken;
    }

    public void setToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
