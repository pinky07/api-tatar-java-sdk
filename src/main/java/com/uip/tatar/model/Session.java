package com.uip.tatar.model;


/**
 * Author: Khamidullin Kamil
 * Date: 04.07.13
 * Time: 22:41
 */
public interface Session {
    public String getToken();
}
