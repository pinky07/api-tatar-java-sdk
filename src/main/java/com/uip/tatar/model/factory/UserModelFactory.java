package com.uip.tatar.model.factory;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.uip.tatar.api.APITatarResponse;
import com.uip.tatar.api.ResultHandler;
import com.uip.tatar.api.WrongCredentialsException;
import com.uip.tatar.model.user.User;
import com.uip.tatar.util.StringTypeAdapter;
import org.json.JSONArray;
import org.json.JSONObject;
import static com.uip.tatar.util.JsonUtils.json2Object;

/**
 * Author: Khamidullin Kamil
 * Date: 02.06.14
 * Time: 14:34
 */
public class UserModelFactory implements ResultHandler<User> {
    private boolean parseFromArray = false;

    public UserModelFactory() {}

    public UserModelFactory(boolean parseFromArray) {
        this.parseFromArray = parseFromArray;
    }

    @Override
    public User handleResult(APITatarResponse response) throws Exception {
        JSONObject userData;
        if(parseFromArray) {
            JSONArray resp  = new JSONArray(response.getData());
            if(resp.length() == 0)  {
                throw new WrongCredentialsException();
            }
            userData = (JSONObject)resp.get(0);
        } else {
            userData = new JSONObject(response.getData());
        }

        return new GsonBuilder()
                .registerTypeAdapter(String.class, new StringTypeAdapter())
                .setDateFormat("yyyy-MM-dd")
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
                .fromJson(userData.toString(), User.class);
    }
}
