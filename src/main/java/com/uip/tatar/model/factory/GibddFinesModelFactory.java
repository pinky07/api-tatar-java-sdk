package com.uip.tatar.model.factory;

import com.google.gson.FieldNamingPolicy;
import com.uip.tatar.api.APITatarResponse;
import com.uip.tatar.api.ResultHandler;
import com.uip.tatar.model.gibdd.GibddFines;
import org.json.JSONObject;
import static com.uip.tatar.util.JsonUtils.json2Object;

/**
 * Author: Khamidullin Kamil
 * Date: 02.06.14
 * Time: 17:47
 */
public class GibddFinesModelFactory implements ResultHandler<GibddFines> {
    @Override
    public GibddFines handleResult(APITatarResponse response) throws Exception {
        JSONObject result   = new JSONObject(response.getData());
        return json2Object(
                result.get("fines").toString(),
                GibddFines.class,
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES
        );
    }
}
