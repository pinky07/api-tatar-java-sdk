package com.uip.tatar.model.gibdd;

import com.uip.tatar.util.StringUtils;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 30.05.14
 * Time: 10:59
 */
public class GibddFine implements Serializable {
    private static final long serialVersionUID = -5183480703471984884L;

    private String protocolRegion;
    private String protocolSeries;
    private String protocolNumber;
    private String date;
    private double amount;
    private String koap;

    public String getProtocolRegion() {
        return protocolRegion;
    }

    public String getProtocolSeries() {
        return protocolSeries;
    }

    public String getProtocolNumber() {
        return protocolNumber;
    }

    public String getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public String getKoap() {
        return koap;
    }
}
