package com.uip.tatar.model.history;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 18.06.14
 * Time: 11:15
 */
public class PaymentHistory implements Serializable {
    private static final long serialVersionUID = -3641800953101147844L;

    private String payment_id;
    private String request_code;
    private String create_date;
    private String title;
    private double sum;
    private String quittance_url;

    public String getPaymentId() {
        return payment_id;
    }

    public String getRequestCode() {
        return request_code;
    }

    public String getCreateDate() {
        return create_date;
    }

    public String getTitle() {
        return title;
    }

    public double getSum() {
        return sum;
    }

    public String getQuittanceUrl() {
        return quittance_url;
    }
}
