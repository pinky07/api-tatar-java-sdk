package com.uip.tatar.model.history;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 18.06.14
 * Time: 11:17
 */
public class AppointmentHistory implements Serializable {
    private static final long serialVersionUID = 1053610693868096458L;

    private String request_id;
    private String request_code;
    private String create_date;
    private String title;
    private String memo_url;

    public String getRequestId() {
        return request_id;
    }

    public String getRequestCode() {
        return request_code;
    }

    public String getCreateDate() {
        return create_date;
    }

    public String getTitle() {
        return title;
    }

    public String getMemoUrl() {
        return memo_url;
    }
}
