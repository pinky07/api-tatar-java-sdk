package com.uip.tatar.api;


import com.uip.tatar.APITatarException;
import com.uip.tatar.auth.AuthSupport;
import com.uip.tatar.auth.Authorization;
import com.uip.tatar.config.Configuration;

/**
 * Author: Khamidullin Kamil
 * Date: 25.12.13
 * Time: 10:18
 */
final public class APITatarAuthClient extends APITatarDefaultClient {
    protected Authorization authorization;

    public APITatarAuthClient(Configuration configuration, Authorization authorization) {
        super(configuration);
        this.authorization = authorization;

        if(authorization instanceof AuthSupport) {
            setErrorHandler(new UnauthorizationHandler((AuthSupport)authorization));
        }
    }

    private void ensureAuthorizationEnabled(APITatarRequest request) {
        if(!authorization.isEnabled() && (request instanceof APITatarAuthRequest)) {
            throw new IllegalStateException("Authorization credentials are missing");
        }
    }

    @Override
    final protected <T> T send(APITatarRequest request, ResultHandler<T> handler, ErrorHandler errorHandler) throws APITatarException {
        ensureAuthorizationEnabled(request);
        try {
            return super.send(request, handler, errorHandler);
        } catch (NeedRetryRequestException exception) {
            return super.send(request, handler, errorHandler);
        }
    }

    @Override
    final protected void onPrepareRequest(APITatarRequest request) {
        super.onPrepareRequest(request);

        if(request instanceof APITatarAuthRequest) {
            final String token = this.authorization
                    .getSession()
                    .getToken();

            ((APITatarAuthRequest)request).setToken(token);
        }
    }

    /**
     * Класс обработчик, отвечающий за поддержку сессиии
     */
    final private class UnauthorizationHandler extends DefaultErrorHandler {
        protected AuthSupport authorization;

        public UnauthorizationHandler(AuthSupport authorization) {
            this.authorization = authorization;
        }

        @Override
        public APITatarException createException(APITatarResponse response) {
            APITatarException exception = super.createException(response);
            if(exception instanceof UnauthorizationException) {
                try {
                    this.authorization.updateSession();
                    return new NeedRetryRequestException();
                } catch (APITatarException e) {
                    return e;
                }
            } else {
                return exception;
            }
        }
    }

    private final static class NeedRetryRequestException extends APITatarException {
        private static final long serialVersionUID = -2119298069915954400L;
    }
}
