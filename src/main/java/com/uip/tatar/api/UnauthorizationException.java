package com.uip.tatar.api;

import com.uip.tatar.APITatarException;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 21.02.13
 * Time: 12:16
 */
public class UnauthorizationException extends APITatarException {
    private static final long serialVersionUID = 2816675871442159156L;
}
