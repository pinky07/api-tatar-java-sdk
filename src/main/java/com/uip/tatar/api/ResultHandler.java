package com.uip.tatar.api;

/**
 * Author: Khamidullin Kamil
 * Date: 02.06.14
 * Time: 14:37
 */
public interface ResultHandler<Result> {
    public Result handleResult(APITatarResponse response) throws Exception;
}
