package com.uip.tatar.api;

import com.uip.tatar.APITatarException;
import com.uip.tatar.DefaultAPITatarImpl;
import com.uip.tatar.config.Configuration;
import com.uip.tatar.config.ConfigurationBuilder;

/**
 * Author: Khamidullin Kamil
 * Date: 11.06.14
 * Time: 15:03
 */
public class APITatarRequestExecutorImpl implements APITatarRequestExecutor {
    private APITatarAuthClient authClient;
    private APITatarDefaultClient defaultClient;


    public APITatarRequestExecutorImpl() {

    }

    private Configuration buildConfiguration() {
        //new ConfigurationBuilder

        return null;
    }

    private APITatarDefaultClient clientFor(APITatarRequest request) {
        if(request instanceof APITatarAuthRequest) {
            return  authClient;
        }

        return defaultClient;
    }

    @Override
    public <T> T get(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException {
        return clientFor(request).get(request, resultHandler);
    }

    @Override
    public <T> T post(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException {
        return clientFor(request).post(request, resultHandler);
    }

    @Override
    public <T> T put(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException {
        return clientFor(request).put(request, resultHandler);
    }

    @Override
    public <T> T delete(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException {
        return clientFor(request).delete(request, resultHandler);
    }
}
