package com.uip.tatar.api;

/**
 * Author: Khamidullin Kamil
 * Date: 24.12.13
 * Time: 14:33
 */
public enum APITatarSupportHttpMethod {GET, POST, DELETE, PUT;

    @Override
    public String toString() {
        switch (this) {
            case GET:
                return "GET";
            case POST:
                return "POST";
            case DELETE:
                return "DELETE";
            case PUT:
                return "PUT";
            default:
                return super.toString();
        }
    }
}
