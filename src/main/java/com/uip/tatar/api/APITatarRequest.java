package com.uip.tatar.api;



import com.uip.tatar.network.HttpQueryParams;
import com.uip.tatar.network.JsonHttpEntity;

import java.util.HashMap;

/**
 * Author: Khamidullin Kamil
 * Date: 24.12.13
 * Time: 14:32
 */
public class APITatarRequest {
    private String apiUrl;
    private String resource;
    private HttpQueryParams query;
    private JsonHttpEntity content;
    private HashMap<String, String> headers;
    private APITatarSupportHttpMethod httpMethod = APITatarSupportHttpMethod.GET;
    private final static String FORMAT = "json";

    private final static String REQUEST_DEBUG_LOG_PATTERN = "{'REQUEST' : {'HTTP_METHOD':'%s', 'URL':'%s', 'ENTITY': %s}}";

    public APITatarRequest(String resource) {
        setResource(resource);
        this.headers    = new HashMap<String, String>();
        this.query      = new HttpQueryParams();
        this.content    = new JsonHttpEntity();
    }

    public APITatarRequest(String resource, HttpQueryParams query) {
        setResource(resource);
        this.query      = query;
        this.headers    = new HashMap<String, String>();
        this.content    = new JsonHttpEntity();
    }

    public APITatarRequest(String resource, HashMap<String, String> headers) {
        setResource(resource);
        this.headers    = headers;
        this.query      = new HttpQueryParams();
        this.content    = new JsonHttpEntity();
    }

    public APITatarRequest(String resource, HttpQueryParams query, JsonHttpEntity content, HashMap<String, String> headers) {
        setResource(resource);
        this.query      = query;
        this.content    = content;
        this.headers    = headers;
    }

    public APITatarRequest(String resource, HttpQueryParams query, HashMap<String, String> headers) {
        setResource(resource);
        this.query      = query;
        this.headers    = headers;
        this.content    = new JsonHttpEntity();
    }

    public APITatarRequest(String resource, JsonHttpEntity content) {
        setResource(resource);
        this.query      = new HttpQueryParams();
        this.content    = content;
        this.headers    = new HashMap<String, String>();
    }

    public APITatarRequest(String resource, JsonHttpEntity content, HashMap<String, String> headers) {
        setResource(resource);
        this.query      = new HttpQueryParams();
        this.content    = content;
        this.headers    = headers;
    }

    public APITatarSupportHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(APITatarSupportHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getResourceURI() {
        return resource;
    }

    public String getResourceURIWithQueryString() {
        return getResourceURI() + getQuery().getQueryString();
    }

    public void setResource(String resource) {
        if(!resource.contains(FORMAT))
            this.resource = resource + "." + FORMAT;
        else
            this.resource = resource;
    }

    public HttpQueryParams getQuery() {
        return query;
    }

    public void setQuery(HttpQueryParams query) {
        this.query = query;
    }

    public JsonHttpEntity getContent() {
        return content;
    }

    public void setContent(JsonHttpEntity content) {
        this.content = content;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void addHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public void addQueryString(String key, String value) {
        this.query.put(key, value);
    }

    public void sign(String signature) {
        headers.put(APITatarConstants.REQUEST_SIGNATURE_HEADER, signature);
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String buildFullResourceUrl() {
        return String.format("%s/%s", apiUrl, getResourceURIWithQueryString());
    }

    @Override
    public String toString() {
        return String.format(REQUEST_DEBUG_LOG_PATTERN,
                getHttpMethod().toString(),
                buildFullResourceUrl(),
                (getContent().getContentLength() > 0) ? getContent() : ""
        );
    }
}
