package com.uip.tatar.api;

/**
 * Author: Khamidullin Kamil
 * Date: 02.02.13
 * Time: 18:17
 */
public interface APITatarConstants {
    String CHARSET_UTF8         = "UTF-8";
    String CHARSET_ISO_8859_1   = "ISO-8859-1";
    String DEFAULT_CHARSET      = CHARSET_UTF8;

    String PROTOCOL_HTTPS       = "https";
    String PROTOCOL_HTTP        = "http";
    String URL_DELIMITER        = "/";

    String JSON_EXTENSION       = ".json";

    String API_DEFAULT_EXTENSION= JSON_EXTENSION;
    String SESSION_TOKEN        = "session_token";
    String AUTH_TOKEN_HEADER    = "x-authorization-token";
    String REQUEST_SIGNATURE_HEADER = "x-request-signature";

    String SEGMENT_SERVICES     = "services";

    String SEGMENT_SERVICE      = "service";
    String SEGMENT_GIBDD        = "gibdd";
    String SEGMENT_FINES_REQUEST= "fines-requests";
    String SEGMENT_FINES        = "fines";
    String SEGMENT_TAXES        = "tax-debts";
    String SEGMENT_TAXES_REQUEST= "tax-debts-requests";
    String SEGMENT_FNS          = "fns";
    String SEGMENT_JKH          = "hcs";
    String SEGMENT_TSJ          = "tsj";
    String SEGMENT_TSJ_REQUEST  = "tsj-requests";
    String SEGMENT_OPERATION_HISTORY  = "operations-history";
    String SEGMENT_PAYMENTS     = "payments";
}