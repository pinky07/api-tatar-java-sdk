package com.uip.tatar.api;

import com.uip.tatar.APITatarException;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 17.03.13
 * Time: 18:40
 */
public class WrongCredentialsException extends APITatarException {
}
