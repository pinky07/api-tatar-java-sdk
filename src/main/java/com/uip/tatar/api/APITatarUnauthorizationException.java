package com.uip.tatar.api;


import com.uip.tatar.api.UnauthorizationException;

/**
 * Author: Khamidullin Kamil
 * Date: 06.03.13
 * Time: 17:40
 */
public class APITatarUnauthorizationException extends UnauthorizationException {
}
