package com.uip.tatar.api;

import com.uip.tatar.APITatarException;

/**
 * Author: Khamidullin Kamil
 * Date: 11.06.14
 * Time: 14:47
 */
public interface APITatarRequestExecutor {
    public <T> T get(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException;

    public <T> T post(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException;

    public <T> T put(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException;

    public <T> T delete(APITatarRequest request, ResultHandler<T> resultHandler) throws APITatarException;
}
