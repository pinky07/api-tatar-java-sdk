package com.uip.tatar.api;


import com.uip.tatar.api.WrongCredentialsException;

/**
 * Author: Khamidullin Kamil
 * Date: 30.01.14
 * Time: 17:18
 */
public class APITatarWrongCredentialsException extends WrongCredentialsException {
}
