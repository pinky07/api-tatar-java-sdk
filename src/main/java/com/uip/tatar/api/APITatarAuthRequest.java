package com.uip.tatar.api;

import com.uip.tatar.network.HttpQueryParams;
import com.uip.tatar.network.JsonHttpEntity;

import java.util.HashMap;

/**
 * Author: Khamidullin Kamil
 * Date: 11.06.14
 * Time: 14:52
 */
final public class APITatarAuthRequest extends APITatarRequest {
    private final static String AUTHORIZATION_PROXY = "authorize-proxy:api:v1:";

    public APITatarAuthRequest(String resourceUri) {
        super(resourceUri);
    }

    public APITatarAuthRequest(String resource, HttpQueryParams query) {
        super(resource, query);
    }

    public APITatarAuthRequest(String resource, HashMap<String, String> headers) {
        super(resource, headers);
    }

    public APITatarAuthRequest(String resource, HttpQueryParams query, JsonHttpEntity content, HashMap<String, String> headers) {
        super(resource, query, content, headers);
    }

    public APITatarAuthRequest(String resource, HttpQueryParams query, HashMap<String, String> headers) {
        super(resource, query, headers);
    }

    public APITatarAuthRequest(String resource, JsonHttpEntity content) {
        super(resource, content);
    }

    public APITatarAuthRequest(String resource, JsonHttpEntity content, HashMap<String, String> headers) {
        super(resource, content, headers);
    }

    public String getResourceURIWithQueryString() {
        String resource = getResourceURI().replaceAll("/", ":");
        return AUTHORIZATION_PROXY + resource + getQuery().getQueryString();
    }

    public void setToken(String token) {
        if(getHttpMethod() == APITatarSupportHttpMethod.GET || getHttpMethod() == APITatarSupportHttpMethod.DELETE) {
            addQueryString(APITatarConstants.SESSION_TOKEN, token);
        } else {
            getContent().put(APITatarConstants.SESSION_TOKEN, token);
        }
    }
}
