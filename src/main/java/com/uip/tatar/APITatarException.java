package com.uip.tatar;

import org.apache.http.HttpResponse;

/**
 * Author: Khamidullin Kamil
 * Date: 29.05.14
 * Time: 11:21
 */
public class APITatarException extends Exception {
    private static final long serialVersionUID = -1085690682119935749L;
    private int statusCode = -1;
    private String message;

    public APITatarException() {
    }

    public APITatarException(String message) {
        super(message);
        this.message = message;
    }

    public APITatarException(String message, HttpResponse httpResponse) {
        super(message);
        this.message = message;
        this.statusCode = httpResponse
                .getStatusLine()
                .getStatusCode();
    }

    public APITatarException(Exception exception, HttpResponse httpResponse) {
        super(exception);
        this.message = (exception.getMessage() != null) ?
                exception.getMessage() :
                httpResponse.getStatusLine().getReasonPhrase();
        this.statusCode = httpResponse
                .getStatusLine()
                .getStatusCode();
    }

    public APITatarException(HttpResponse httpResponse) {
        super(httpResponse.getStatusLine().getReasonPhrase());
        this.message = httpResponse
                .getStatusLine()
                .getReasonPhrase();
        this.statusCode = httpResponse
                .getStatusLine()
                .getStatusCode();
    }

    public APITatarException(Exception exception) {
        super(exception);
        this.message = exception.getMessage();
    }
}
