package com.uip.tatar.sample;

import com.uip.tatar.APITatar;
import com.uip.tatar.APITatarException;
import com.uip.tatar.APITatarFactory;
import com.uip.tatar.config.*;
import com.uip.tatar.model.gibdd.GibddFines;
import com.uip.tatar.model.history.Paginate;
import com.uip.tatar.model.history.PaymentHistories;
import com.uip.tatar.model.user.User;
import com.uip.tatar.model.user.Vehicle;

/**
 * Author: Khamidullin Kamil
 * Date: 05.02.14
 * Time: 10:51
 */
public class RequestSample {
    public static void main(String[] args) {
        try {
            //Создаем настройки для апи клиента
            Configuration apiTatarConfiguration = new ConfigurationBuilder()
                    .url("api.tatar.ru") //default
                    .useSSL(true)
                    .version("v1")  //default
                    .debug(true)
                    .authorizationToken("1d4c9c2443d0e902c2bb0aa98dd6221b87b71be81ffd1de4b7ce410f9872bf8a33c627d48606baad")  //default
                    .secretKey("f8dc24763533a2854c5947ca190169a35f1f41e2077b39ff15aeb6a10ee3b5c8219815f56e66746f")  //default
                    .serverDoesNotRespondErrorText("Сервер не отвечает") //default
                    .wrongServerResponseErrorText("Неправильный формат ответа сервера") //default
                    .build();

            APITatar apiTatar =  APITatarFactory.getInstance(apiTatarConfiguration);

            //auth
            apiTatar.auth("9518976217", "1");

            //get user
            User user =  apiTatar.getUser("9518976217", "1");

            user.setLastName("blablabla");
            Vehicle vehicle =  user.getDefaultVehicle();
            user.removeVehicle(vehicle);

            //update
            apiTatar.updateUser(user);

            //get fines
            GibddFines fines = apiTatar.getFines(user.getDefaultVehicle());

            //get payment histories
            PaymentHistories paymentHistories = apiTatar.getPaymentHistories(user.getId(), new Paginate());

            //send sms
            apiTatar.sendSMS(user.getId(), "message text");


            //System.out.println(fines);

        } catch (APITatarException iex) {
            System.out.println(iex.getMessage());
        }
    }
}
