package com.uip.tatar.util;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;

/**
 * Author: Khamidullin Kamil
 * Date: 02.06.14
 * Time: 14:41
 */
public class JsonUtils {
    public static  <T> T json2Object(String json, Class<T> cls, FieldNamingPolicy namingPolicy) {
        return new GsonBuilder()
                .registerTypeAdapter(String.class, new StringTypeAdapter())
                .serializeNulls()
                .setFieldNamingPolicy(namingPolicy)
                .create()
                .fromJson(json, cls);
    }

    public static <T> T json2Object(String json, Class<T> cls) {
        return new GsonBuilder().create()
                .fromJson(json, cls);
    }
}
