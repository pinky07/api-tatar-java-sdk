package com.uip.tatar.util;

import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Khamidullin Kamil
 * Date: 04.02.14
 * Time: 13:34
 */
public class StringUtils {

    /**
     * Генерирует строку в sha1
     * @param string
     * @return
     */
    public static String toSHA1(String string) {
        try {
            MessageDigest cript = MessageDigest.getInstance("SHA-1");
            cript.reset();
            cript.update(string.getBytes("UTF-8"));
            return new String(Hex.encodeHex(cript.digest()));
        } catch (NoSuchAlgorithmException unencex) {
            /*sha - 1 always available*/ return "";
        } catch (UnsupportedEncodingException unencex) {
            /*UTF-8 always available*/ return "";
        }
    }

    public static boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public static String latinizeAutoNumber(String number) {
        char[] autoNumberCharsRus = {
                'а', 'в', 'к', 'у',
                'н', 'е', 'х', 'р',
                'о', 'с', 'м', 'т'
        };
        char[] autoNumberCharsEng = {
                'a', 'b', 'k', 'y',
                'h', 'e', 'x', 'p',
                'o', 'c', 'm', 't'
        };

        number = number.toLowerCase();

        for (int i = 0; i < autoNumberCharsRus.length; i++) {
            number = number.replace(autoNumberCharsRus[i], autoNumberCharsEng[i]);
        }

        return number.toUpperCase();
    }
}
