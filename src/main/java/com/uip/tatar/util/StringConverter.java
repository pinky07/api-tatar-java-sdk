package com.uip.tatar.util;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Author: Khamidullin Kamil
 * Date: 03.06.14
 * Time: 10:51
 */
public class StringConverter implements JsonSerializer<String>,
        JsonDeserializer<String> {
    public JsonElement serialize(String src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        if (src == null) {
            return new JsonPrimitive("");
        } else {
            return new JsonPrimitive(src);
        }
    }

    public String deserialize(JsonElement json, Type typeOfT,
                              JsonDeserializationContext context)
            throws JsonParseException {
        return json.getAsJsonPrimitive().getAsString();
    }
}
