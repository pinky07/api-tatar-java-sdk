# API tatar java sdk
## Пример работы
**Создаем настройки**
``` java
APITatarConfiguration apiTatarConfiguration = new APITatarConfiguration.Builder()
	.url("api.tatar.ru") //default
	.debug(true) //print request/response source data | default false
	.version("v1")  //default
	.authorizationToken("1d4c9c2443d0e902c2bb0aa98dd6221b87b71be81ffd1de4b7ce410f9872bf8a33c627d48606baad")  //default
	.secretKey("f8dc24763533a2854c5947ca190169a35f1f41e2077b39ff15aeb6a10ee3b5c8219815f56e66746f")  //default
	.serverDoesNotRespondErrorText("Сервер не отвечает") //default
	.wrongServerResponseErrorText("Неправильный формат ответа сервера") //default
	.build();
```
**Формирование запроса на API**
``` java
String resource = "sessions.json";

JsonHttpEntity userAuthParams = new JsonHttpEntity();
userAuthParams.put("username", "9518976217");
userAuthParams.put("password", "1");

new APITatarRequest(resource, userAuthParams);
```
**Отправка запроса на API и обработка ответа**
<br />При обработке ответа используя стандартный обработчик, необходимо указать класс модели, на которую мапятся данные.<br />
``` java 
 
//Создаем апи клиент
APITatarDefaultClient apiClient = new APITatarDefaultClient(apiTatarConfiguration);

//Создаем делегат - обработчик ответа
ResultHandler<APITatarSession> handler = new DefaultResultHandler<APITatarSession>(APITatarSession.class, true);

//посылаем запрос методом POST и получаем в ответ модель с данными
APITatarSession session = apiClient.post(request, handler);
```
**Нестандартная обработка ответа**
<br />При отсутствие какой либо модели, либо при необходимости нестандартной обработки ответа сервера, можно создать свой собственный обработчик, который должен реализовывать интерфейс ResultHandler.<br />
```java 
ResultHandler<APITatarSession> customHandler = ResultHandler<APITatarSession>() {
	@Override
	public APITatarSession handleResult(String resultString) throws IOException, JSONException {
		    JSONObject jsonResponse = new JSONObject(resultString);
		    APITatarSession session = new APITatarSession();
		    session.setToken(jsonResponse.getString("session_token"));
		
		    return session;
		}
	}
```
**Авторизованный API клиент**
<br />При создании в конструктор необходимо передать объект сессии, принцип работы клиента тот же.<br />
```java 
APITatarSession session = new APITatarSession();
session.setToken("YOUR_AUTH_TOKEN");

//Создаем апи клиент
APITatarDefaultClient apiClient = new APITatarAuthClient(apiTatarConfiguration, session);
```
